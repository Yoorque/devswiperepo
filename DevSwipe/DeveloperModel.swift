//
//  DeveloperModel.swift
//  DevSwipe
//
//  Created by Dusan Juranovic on 8/16/17.
//  Copyright © 2017 Dusan Juranovic. All rights reserved.
//

import Foundation
import UIKit

var devsArray = [Developer(name: "Clara Smith", sex: "F", skill: "Django"),
                 Developer(name: "John Harper", sex: "M", skill: "Phyton"),Developer(name: "Sagar Shah", sex: "M", skill: "Django"),Developer(name: "Bob Thomas", sex: "M", skill: "React"),Developer(name: "Sophie Laura", sex: "F", skill: "React"),Developer(name: "Sammie Lopez", sex: "F", skill: "Django"),Developer(name: "Connie Jones", sex: "F", skill: "Django"),Developer(name: "Camille Rowe", sex: "F", skill: "Django"),Developer(name: "Joana Silver", sex: "F", skill: "Phyton"),Developer(name: "Sasha Doe", sex: "F", skill: "Django"),Developer(name: "Gaby Simone", sex: "F", skill: "React"),Developer(name: "Chloe Isabella", sex: "F", skill: "React"),Developer(name: "George Stanley", sex: "M", skill: "Django"),Developer(name: "Dominic Hope", sex: "M", skill: "Django"),Developer(name: "Sandra Bonhomme", sex: "F", skill: "Django"),Developer(name: "Jay Jiang", sex: "M", skill: "Django"),Developer(name: "Hunter Dickson", sex: "M", skill: "React"),Developer(name: "Tom Brady", sex: "M", skill: "Django"),Developer(name: "Eric Street", sex: "M", skill: "Phyton"),Developer(name: "Ronald Duck", sex: "M", skill: "Phyton"), Developer(name: "Sophie Gerbault", sex: "F", skill: "React"), Developer(name: "Sarah Seemore", sex: "F", skill: "React"), Developer(name: "Rachel Green", sex: "F", skill: "Django"), Developer(name: "Jospehine Taylor", sex: "F", skill: "Django"), Developer(name: "Taylor Thomson", sex: "F", skill: "Django"), Developer(name: "Tatiana Marshall", sex: "F", skill: "Django"), Developer(name: "Marine Simone", sex: "F", skill: "Phyton"), Developer(name: "Jennifer Lopez", sex: "F", skill: "Phyton"), Developer(name: "Sam Gupta", sex: "M", skill: "React")]

class Developer {
    var name: String
    var sex: String
    var skill: String
    
    init(name: String, sex: String, skill: String) {
        self.name = name
        self.sex = sex
        self.skill = skill
    }
}
