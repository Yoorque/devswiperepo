    //
//  ViewController.swift
//  DevSwipe
//
//  Created by Dusan Juranovic on 8/16/17.
//  Copyright © 2017 Dusan Juranovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var tableView: UITableView!
    var currentDev: Developer?
    var selectedCandidatesArray = [Developer]()
    @IBOutlet var sexLabel: UILabel!
    @IBOutlet var skillLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var devImageView: UIImageView!
    @IBOutlet var devView: UIView!
    @IBOutlet var yesOrNoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDevImage()
        
        let leftSwiper = UISwipeGestureRecognizer(target: self, action: #selector(swiper))
        leftSwiper.direction = .left
        let rightSwiper = UISwipeGestureRecognizer(target: self, action: #selector(swiper))
        rightSwiper.direction = .right
        devView.addGestureRecognizer(leftSwiper)
        devView.addGestureRecognizer(rightSwiper)
        
    }

    func swiper(sender: UISwipeGestureRecognizer) {
        if let view = sender.view {
            if sender.direction == .left {
                self.yesOrNoLabel.textColor = .red
                self.yesOrNoLabel.text = "NOPE"
                
                UIView.animate(withDuration: 0.5, animations: {_ in
                view.transform = CGAffineTransform(translationX: -self.devView.frame.size.width, y: self.devView.frame.size.height / 2)
                    
                }, completion: {_ in
                    self.loadDevImage()
                    view.transform = .identity
                })
                
            } else if sender.direction == .right {
                selectedCandidatesArray.append(currentDev!)
                tableView.reloadData()
                self.yesOrNoLabel.textColor = .green
                self.yesOrNoLabel.text = "YUP"
                
                UIView.animate(withDuration: 0.5, animations: {_ in
                view.transform = CGAffineTransform(translationX: self.devView.frame.size.width, y: self.devView.frame.size.height / 2)
                    
                }, completion: {_ in
                    self.loadDevImage()
                    view.transform = .identity
                })
            }
        }
    }
    
    func loadDevImage() {
        let randomNumber = Int(arc4random_uniform(UInt32(devsArray.count - 1)))
        yesOrNoLabel.text = ""
        devImageView.image = UIImage(named: "\(devsArray[randomNumber].sex)")
        if devsArray[randomNumber].sex == "M" {
            sexLabel.text = "M"
            sexLabel.textColor = .blue
            skillLabel.textColor = .blue
            nameLabel.textColor = .blue
        } else {
            sexLabel.text = "F"
            sexLabel.textColor = .red
            skillLabel.textColor = .red
            nameLabel.textColor = .red
        }
        skillLabel.text = devsArray[randomNumber].skill
        nameLabel.text = devsArray[randomNumber].name
        currentDev = devsArray[randomNumber]
    }
}
    
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return selectedCandidatesArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = selectedCandidatesArray[indexPath.row].name + " " + selectedCandidatesArray[indexPath.row].sex
            cell.detailTextLabel?.text = selectedCandidatesArray[indexPath.row].skill
            return cell
        }
    }

